import { createRouter, createWebHistory } from "vue-router";
import App from './App.vue';

import LogIn from './components/LogIn.vue'
import CreateUser from './components/CreateUser.vue'
import CreateProduct from './components/CreateProduct.vue'
import CreateSale from './components/CreateSale.vue'
import Home from './components/Home.vue'
import HomeUser from './components/HomeUser.vue'
import Users from './components/Users.vue'
import Products from './components/Products.vue'
import ProductsC from './components/ProductsC.vue'
import Sale from './components/Sale.vue'
import Sales from './components/Sales.vue'
import UserUpdate from './components/UserUpdate.vue'
import ProductUpdate from './components/ProductUpdate.vue'

const routes = [{
        path: '/',
        name: 'root',
        component: App
    },
    {
        path: '/home',
        name: "home",
        component: Home
    },
    {
        path: '/user/logIn',
        name: "logIn",
        component: LogIn
    },
    {
        path: '/user/logIn',
        name: "logIn",
        component: LogIn
    },    
    {
        path: '/user/home',
        name: "homeUser",
        component: HomeUser
    },
    {
        path: '/user/createUser',
        name: "createUser",
        component: CreateUser
    },
    {
        path: '/user/users',
        name: "users",
        component: Users
    },
    {
        path: '/user/createProduct',
        name: "createProduct",
        component: CreateProduct
    },
    {
        path: '/user/products',
        name: "products",
        component: Products
    },
    {
        path: '/products',
        name: "productsC",
        component: ProductsC
    },
    {
        path: '/sale',
        name: "sale",
        component: Sale
    },
    {
        path: '/user/sales',
        name: "sales",
        component: Sales
    },
    {
        path: '/user/createSale',
        name: "createSale",
        component: CreateSale
    },
    {
        path: '/user/userUpdate',
        name: "userUpdate",
        component: UserUpdate
    },
    {
        path: '/user/productUpdate',
        name: "productUpdate",
        component: ProductUpdate
    },
];
const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;